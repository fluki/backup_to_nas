#!/bin/bash/

# 0 -> OK
# 1 -> WNG
# 2 -> ERR
# 3 -> UNK

LOG_FILE=/var/log/backup.log
FOLDER=
LOGIN=
BACKUP_SERVER_NAME=
BACKUP_SERVER_IP=
exec &>$LOG_FILE

########### FUNCTIONS ##########

#Function which will check if your folder exist
function FolderFinder() {
  find /home/$LOGIN -type f -name $FOLDER
  if [[ $? -eq 0 ]]; then
    echo " the folder exist"
    exit 0
  else
    echo "The folder doesn't exist"
    exit 1
  fi
}

#Function in order to check if NAS is reachable
function ConnectionTest() {
  dns=`nslookup $BACKUP_SERVER_NAME`
  dnsresult=`nslookup $BACKUP_SERVER_NAME |grep $BACKUP_SERVER_IP`

  #If NAS IP isn't present in the nslookup command
  if [[ $dnsresult =! "$BACKUP_SERVER_IP" ]]; then

    #Connectivity test with ping
    ping -c 4 $BACKUP_SERVER_IP
    pingresult=$?

    #If ping works
    if [[ $pingresult -eq 0 ]]; then
      #echo WARNING
      echo "WARNING: check your dns configuration"
      exit 1
    #Else network issues
    else
      echo "CRITICAL: Check the network configuration of your NAS"
      echo "CRITICAL: $BACKUP_SERVER_NAME is unreachable"
      exit 2
    fi

  else
    echo "Connection Test completed"
    exit 0
  fi
}

#Copy the all folder
function Copy() {
  #
}

#Check if the folder is in the NAS
function CopyChecker() {
  #
}

############ MAIN #############

FolderFinder
if [[ $? -eq 0 ]]; then
  ConnectionTest
  if [ $? -eq 0 ] or [ $? -eq 1 ]; then
    #statements
    Copy
  fi
fi
